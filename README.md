# ADIOS

Anomaly Detection and Intrusion Observing System
---
It use Argus (https://qosient.com/argus/) to collect traffic data. To start `argus` type in terminal
```user@user# argus -J -w argus-udp://127.0.0.1:7777```

`report_system.py` collects data from argus, generate features and save it to database `database/db.db`.

`web_and_classification.py` gets data from database and use OneClass SVM (sklearn) to classify network events. 
It use Plotly Dash to create web interface (see 127.0.0.1:8050).