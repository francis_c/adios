import subprocess
import numpy as np
import time
import scipy.special
from datetime import datetime
from db_utils import *
import matplotlib.pyplot as plt


def get_entropy(arr):
    arr = arr[arr != 0]
    S = sum(arr)
    arr = arr / S
    return -sum(arr*np.log2(arr))


# ra -nn -S argus-udp://127.0.0.1:7777 -M uni -F ra.conf -- src or dst host 192.168.0.105
create_database('database/db.db')
ip = '192.168.0.103'
proc = subprocess.Popen(['ra', '-nn', '-S', 'argus-udp://127.0.0.1:7777', '-M', 'uni',
                         '-F', 'ra.conf', '--', 'src or dst host {}'.format(ip)], stdout=subprocess.PIPE)
srcP_X = np.zeros(65537)
dstP_X = np.zeros(65537)
kl_dstP_prev = np.zeros(65537)
kl_srcP_prev = np.zeros(65537)
kl_srcP_X = np.array([np.zeros(65537)])
kl_dstP_X = np.array([np.zeros(65537)])
src_dst_list = []
src_dst_X = []
srcIP_X = []
srcIP_list = []
log = ''
n_pkts = 0
n_conns = 0
n_bytes = 0

start_time = time.time()
h_winsize = 10
last_time = start_time + h_winsize
kl_stride = 10
kl_winsize = 30


def decode(x): return x.decode()


while True:
    line = proc.stdout.readline()
    if line != '':
        record = line.strip().split(b"*")
        if record[2] in [b'1', b'6', b'17']:
            stime, runt, prot, srcIP, srcP, _, dstIP, dstP, state, pkts, octets = list(
                map(decode, record))
            if dstIP == ip:
                tmp = datetime.strptime(stime, "%Y/%m/%d %H:%M:%S.%f")
                tm = time.mktime(tmp.timetuple())
                if tm > last_time:
                    # dstPort
                    kl_dstP_X = np.append(
                        kl_dstP_X, [dstP_X], axis=0)[-int(kl_winsize/kl_stride):]
                    kl_dstP_hist = np.sum(kl_dstP_X, axis=0)
                    kl_dstP_hist = kl_dstP_hist / sum(kl_dstP_hist)
                    kl_dstP = scipy.special.kl_div(kl_dstP_prev, kl_dstP_hist)
                    kl_dstP = sum(kl_dstP[kl_dstP != float('inf')])
                    kl_dstP_prev = kl_dstP_hist
                    dstH = get_entropy(dstP_X)
                    # srcPort
                    kl_srcP_X = np.append(
                        kl_srcP_X, [srcP_X], axis=0)[-int(kl_winsize/kl_stride):]
                    kl_srcP_hist = np.sum(kl_srcP_X, axis=0)
                    kl_srcP_hist = kl_srcP_hist / sum(kl_srcP_hist)
                    kl_srcP = scipy.special.kl_div(kl_srcP_prev, kl_srcP_hist)
                    kl_srcP = sum(kl_srcP[kl_srcP != float('inf')])
                    kl_srcP_prev = kl_srcP_hist
                    srcH = get_entropy(srcP_X)
                    # srcIP
                    srcIP_X = np.array(srcIP_X)
                    srcIPH = get_entropy(srcIP_X)
                    # src_dst
                    src_dst_X = np.array(src_dst_X)
                    src_dst_H = get_entropy(src_dst_X)
                    # save log
                    log_path = 'logs/log_{0}'.format(
                        datetime.fromtimestamp(last_time))
                    f = open(log_path, 'w')
                    f.write(log)
                    f.close()
                    # output
                    save_data('database/db.db', last_time, srcH, dstH, srcIPH, 
                              src_dst_H, kl_dstP, kl_srcP, log_path,
                              n_conns, n_pkts, n_bytes)
                    # null
                    dstP_X = np.array([0] * 65537)
                    srcP_X = np.array([0] * 65537)
                    src_dst_list = []
                    src_dst_X = []
                    srcIP_X = []
                    srcIP_list = []
                    start_time = last_time
                    last_time = start_time + h_winsize
                    log = ''
                    n_pkts = 0
                    n_conns = 0
                    n_bytes = 0

                log += '{}_{}_{}:{}->{}:{}_{}_{}\n'.format(
                    stime, prot, srcIP, srcP, dstIP, dstP, pkts, octets)
                n_conns += 1
                n_pkts += int(pkts)
                n_bytes += int(octets)
                try:
                    dstP_X[int(dstP)] += 1
                except:
                    dstP_X[int(dstP, 16)] += 1
                try:
                    srcP_X[int(srcP)] += 1
                except:
                    srcP_X[int(srcP, 16)] += 1
                if srcIP not in srcIP_list:
                    srcIP_list.append(srcIP)
                    srcIP_X.append(1)
                else:
                    srcIP_X[srcIP_list.index(srcIP)] += 1
                direct = "{}->{}".format(srcP, dstP)
                if direct not in src_dst_list:
                    src_dst_list.append(direct)
                    src_dst_X.append(1)
                else:
                    src_dst_X[src_dst_list.index(direct)] += 1

    else:
        break
