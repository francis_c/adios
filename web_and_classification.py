# -*- coding:utf-8 -*-
import dash
from dash.dependencies import Output, Event, Input
import dash_core_components as dcc
import dash_html_components as html
import plotly
import sqlite3
import pandas as pd
import numpy as np
import pandas
from sklearn.svm import OneClassSVM
from sklearn.neighbors import LocalOutlierFactor


"""
# Dark Colors
app_colors = {
    'background': '#0C0F0A',
    'text': '#FFFFFF',
    'srcport-plot':'#FF9132',
    'kl_src':'#FFC28E',
    'red':'#F74B4B',
    'green':'#7BCC3A',
    'dstport-plot':'#29D434',
    'kl_dst':'#82EA89',
    'tcprtt':'#1F959A',
    'srcip-plot':'#355CBD',
}
"""

app_colors = {
    'background': '#FFFFFF',
    'text': '#0C0F0A',
    'srcport-plot': '#193F61',
    'kl_src': '#FFBA00',
    'dstport-plot': '#BE150E',
    'kl_dst': '#00C90D',
    'tcprtt': '#1F959A',
    'srcip-plot': '#355CBD',
    'red': '#F74B4B',
    'green': '#7BCC3A'
}


data_dict = {
    "H(dstPort)": 1,
    "H(srcPort)": 2,
    "H(srcIP)": 3,
    "H(src/dst)": 4,
    "KL(dstPort)": 5,
    "KL(srcPort)": 6
}

h3_style = {
    'font-size': '2.0rem',
    'font-family': 'Roboto Slab',
    'color': '#007EFF',
    'border-bottom': '1px solid #D8D8D8',
    'margin-top': '3px',
    'margin-left': '10px',
    'margin-bottom': '3px'
}

block_style = {
    'border-radius': '5px',
    'border-width': '1px 1px 0px',
    'border-top-style': 'solid',
    'border-right-style': 'solid',
    'border-bottom-style': 'initial',
    'border-left-style': 'solid',
    'border-top-color': '#D8D8D8',
    'border-right-color': '#D8D8D8',
    'border-bottom-color': 'initial',
    'border-left-color': '#D8D8D8',
    'border-image': 'initial',
    'height': '103%'
}

app = dash.Dash("ADIOS")
app.layout = html.Div([
    html.Div([html.H3("Anomaly Detection and Intrusion Observing System", style=h3_style)]),
    html.Div([
        html.Div([
            html.H3("ПРИЗНАКИ", style=h3_style)
        ]),
        dcc.Dropdown(
            id='graphs-types',
            options=[{'label': s, 'value': s} for s in data_dict.keys()],
            value=["H(dstPort)", "H(srcPort)", "H(srcIP)"],
            multi=True
        ),
        html.Div([
            dcc.Graph(
                id='global-graph',
                animate=False
            )], style={'width': '93%'}
        ),
        html.Div([
            html.Div(id='time-output-container',
                     style={'margin-top': '10px'}),
            dcc.Slider(
                id='time-slider',
                min=60,
                max=1080,
                step=60,
                value=180,
                marks={
                    60: {'label': '10 мин'},
                    1080: {'label': '3 ч'},
                },
                updatemode='drag'
            )], style={'margin-left': '30px', 'margin-right': '10px', 'margin-bottom': '50px'})
        ], style=block_style
    ),

    html.Div([
        html.Div([html.H3("ДЕТЕКТИРОВАНИЕ АНОМАЛИЙ", style=h3_style)]),
        html.Div([
            html.Div(id='v-output-container',
                     style={'margin-top': '10px', 'margin-left': '0%'}),
            dcc.Slider(
                id='v-slider',
                min=10e-5,
                max=1,
                step=10e-3,
                value=10e-3,
                marks={
                    10e-5: {'label': '0'},
                    1: {'label': '1'},
                },
                updatemode='drag'
            )], style={'margin-left': '30px', 'margin-right': '10px'}),
        html.Div([
            dcc.Graph(
                id='bar-graph',
                animate=False
            )]
        )
    ], style=block_style
    ),

    html.Div([
        html.Div(
            id="log-table",
        )], style=block_style
    ),

    dcc.Interval(
        id='graph-update',
        interval=10*1000
    )
    ], style={'padding': '0px 10px 15px 10px',
              'margin-left': 'auto', 'margin-right': 'auto', "width": "95%",
              'boxShadow': '0px 0px 5px 5px rgba(204,204,204,0.4)',
              'font-family': 'Roboto Slab'},
)


def get_fit_predict(nu, delta):
    conn = sqlite3.connect('database/db.db')
    c = conn.cursor()
    df = pd.read_sql("SELECT * FROM 'data'", conn)
    df.sort_values('tm', inplace=True)

    srcP = df.srcP.values
    dstP = df.dstP.values
    srcIP = df.srcIP.values
    src_dst = df.src_dst.values
    kl_dst = df.kl_dst.values
    kl_src = df.kl_src.values
    X_arr = np.array([srcP, dstP, srcIP, src_dst, kl_dst, kl_src]).T
    clf = OneClassSVM(nu=nu)
    clf.fit(X_arr)
    S = clf.decision_function(X_arr[-10:]).T[0]
    A = clf.decision_function(X_arr[-delta:]).T[0]
    N = clf.decision_function(X_arr[-delta:]).T[0]

    X = df.tm.values[-delta:]*1000

    A[A > 0.0] = 0
    N[N <= 0.0] = 0

    return (X, A, N, S)


get_color = {
    True: '#7BCC3A',  # '#002C0D',
    False: '#F74B4B'  # '#270000'
}


def generate_table(df):
    return html.Table(
        className="responsive-table",
        children=[
            html.Thead(
                html.Tr(
                    children=[
                        html.Th('Время'),
                        html.Th('# Соединений'),
                        html.Th('# Пакетов'),
                        html.Th('# Байт'),
                        html.Th('Путь к файлу регистрации'),
                    ],
                    style=h3_style
                )
            ),
            html.Tbody([
                html.Tr(
                    children=[html.Td(data) for data in d[:5]],
                    style={
                        'color': app_colors['text'],
                        'background-color':get_color[d[-1] > 0],
                        'font-family':'Roboto Slab'}
                )
                for d in df])
        ], style={'width': '94%', 'margin-left': '3%', 'font-size': '97%'}
    )


@app.callback(
    Output('global-graph', 'figure'),
    [Input('graphs-types', 'value'), Input('time-slider', 'value')],
    events=[Event('graph-update', 'interval')]
)
def update_graph_global(data_names, delta):
    try:
        conn = sqlite3.connect('database/db.db')
        c = conn.cursor()
        df = pd.read_sql("SELECT * FROM 'data'", conn)
        df.sort_values('tm', inplace=True)

        X = df.tm.values[-delta:]*1000
        Y1 = df.srcP.values[-delta:]
        Y2 = df.dstP.values[-delta:]
        Y3 = df.srcIP.values[-delta:]
        Y4 = df.src_dst.values[-delta:]
        Y5 = df.kl_dst.values[-delta:]
        Y6 = df.kl_src.values[-delta:]

        data_dict = {
            "H(dstPort)": [Y2, 'dstport-plot'],
            "H(srcPort)": [Y1, 'srcport-plot'],
            "H(srcIP)": [Y3, 'srcip-plot'],
            "H(src/dst)": [Y4, 'text'],
            "KL(dstPort)": [Y5, 'kl_dst'],
            "KL(srcPort)": [Y6, 'kl_src']
        }

        graphs = []

        for data_name in data_names:
            data = plotly.graph_objs.Scatter(
                x=X,
                y=data_dict[data_name][0],
                name=data_name,
                mode='lines+markers',
                line=dict(
                    color=(app_colors[data_dict[data_name][1]]),
                    width=2,
                )
            )
            graphs.append(data)

        return {'data': graphs, 'layout': plotly.graph_objs.Layout(
                font={'color': app_colors['text'], 'family': 'Roboto Slab'},
                xaxis=dict(type='date', range=[min(X), max(X)]),
                plot_bgcolor=app_colors['background'],
                paper_bgcolor=app_colors['background'],
                margin={'l': 45, 'r': 20, 'b': 50, 't': 0},
                )}

    except Exception as e:
        with open('errors.txt', 'a') as f:
            f.write(str(e))
            f.write('\n')


@app.callback(Output('time-output-container', 'children'),
              [Input('time-slider', 'value')])
def display_value(value):
    return 'Время: {0} ч {1} мин'.format(value*10 // 3600, value*10//60 % 60)


@app.callback(Output('v-output-container', 'children'),
              [Input('v-slider', 'value')])
def display_value(value):
    return 'Верхняя граница доли ошибок при обучении: {}'.format(value)


@app.callback(
    Output('bar-graph', 'figure'),
    [Input('v-slider', 'value'), Input('time-slider', 'value')],
    events=[Event('graph-update', 'interval')]
)
def update_graph_bar(nu, delta):
    try:
        X, A, N, _ = get_fit_predict(nu, delta)

        data1 = plotly.graph_objs.Bar(
            x=X,
            y=A,
            name='Аномальная активность',
            marker=dict(
                color=(app_colors['red'])
            )
        )

        data2 = plotly.graph_objs.Bar(
            x=X,
            y=N,
            name='Нормальная активность',
            marker=dict(
                color=(app_colors['green'])
            )
        )

        return {'data': [data1, data2], 'layout': plotly.graph_objs.Layout(
                xaxis=dict(type='date'),
                font={'color': app_colors['text'], 'family': 'Roboto Slab'},
                plot_bgcolor=app_colors['background'],
                paper_bgcolor=app_colors['background'],
                margin={'l': 45, 'r': 20, 'b': 50, 't': 20},
                )}

    except Exception as e:
        with open('errors.txt', 'a') as f:
            f.write(str(e))
            f.write('\n')


@app.callback(Output('log-table', 'children'),
              [Input('v-slider', 'value'), Input('time-slider', 'value')],
              events=[Event('graph-update', 'interval')])
def update_table(nu, delta):
    conn = sqlite3.connect('database/db.db')
    c = conn.cursor()
    df = pd.read_sql("SELECT * FROM 'data'", conn)
    df = df[['tm', 'conns', 'pkts', 'octets', 'log_link']][-10:][::-1]
    df['tm'] = pandas.to_datetime(df['tm'], unit='s')

    _, _, _, decision = get_fit_predict(nu, delta)
    df = np.concatenate((np.array(df), np.array([decision[::-1]]).T), axis=1)

    return generate_table(df)


server = app.server

if __name__ == '__main__':
    # external_css = ["https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"]
    external_css = [
        "https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.min.css",
        "https://fonts.googleapis.com/css?family=Roboto Slab:400,400i,700,700i",
        "https://fonts.googleapis.com/css?family=Product+Sans:400,400i,700,700i"
    ]
    for css in external_css:
        app.css.append_css({"external_url": css})

    external_js = [
        'https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js']
    for js in external_js:
        app.scripts.append_script({'external_url': js})
    app.run_server(debug=True, host='0.0.0.0')
