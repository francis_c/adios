import sqlite3


"""
    Database
"""


def create_database(db_file_path):
    conn = sqlite3.connect(db_file_path)
    with conn:
        try:
            cursor = conn.cursor()
            cursor.execute(
                'CREATE TABLE data (\
                    tm REAL UNIQUE, \
                    srcP REAL, \
                    dstP REAL, \
                    srcIP REAL, \
                    src_dst REAL, \
                    kl_dst REAL, \
                    kl_src REAL, \
                    log_link TEXT, \
                    conns REAL, \
                    pkts REAL, \
                    octets REAL)')
        except sqlite3.OperationalError as ex:
            print('Failed to create table: %s', ex)
    conn.close()


def save_data(db_file_path, tm, srcP, dstP, srcIP, src_dst, kl_dst, kl_src, log_link, conns, pkts, octets):
    conn = sqlite3.connect(db_file_path)
    try:
        cursor = conn.cursor()
        cursor.execute(
            'INSERT INTO data VALUES (?,?,?,?,?,?,?,?,?,?,?)', 
            (tm, srcP, dstP, srcIP, src_dst, kl_dst, kl_src, log_link, conns, pkts, octets))
        conn.commit()
    finally:
        conn.close()


def retrieve_data(db_file_path, tm, delta):
    conn = sqlite3.connect(db_file_path)
    try:
        cursor = conn.cursor()
        result = []
        for row in cursor.execute('SELECT * FROM data WHERE tm > delta', (tm, delta, )):
            _, srcP, dstP, srcIP, src_dst, kl_dst, kl_srcl, log_link, conns, pkts, octets = row
            result.append([srcP, dstP, srcIP, src_dst, kl_dst, kl_src, log_link, conns, pkts, octets])
        return result
    finally:
        conn.close()